# HashiStack: Nomad

The Nomad builder configures a [nomad](https://www.nomadproject.io) cluster by installing and configuring the nomad agent in either server or client modes. Provisioning is handled by shell scripts via the [shell-local](https://developer.hashicorp.com/packer/docs/provisioners/shell-local) provisioner as well as the [ansible provisioner](https://developer.hashicorp.com/packer/integrations/hashicorp/ansible).

**<u>Configurations</u>** 
Base configurations are managed by the `packer/nomad/variables.pkrvars.hcl` file. Ensure all variables which don't have a default value have been set using the file `variables-override.auto.pkrvars.hcl` in the directory `packer/nomad`.

In addition, some configurations are stored in the [1password](https://1password.com) vault `Hashistack`. The configuration are pulled from 1password using a 1password [service account](https://developer.1password.com/docs/service-accounts/) and the 1password [cli utility](https://developer.1password.com/docs/service-accounts/use-with-1password-cli).

**<u>Running the provisioner</u>**
All commands should be executed fromt he root directory.

1. Install all required dependencies locally:
   - [docker](https://docs.docker.com/get-docker/)
   - [packer](https://developer.hashicorp.com/packer/install?product_intent=packer)

2. Ensure the [docker-nomad]([../docker/README.md](https://gitlab.com/all_d245_projects/infrastructure/hashistack/docker/-/blob/dev/packer/docker-nomad/README.md)) image has been built so the image is available locally.

3. Install the requried packer plugins.
   ```bash
   docker run \
   --rm \
   -v ${PWD}:/project_root \
   hashistack/docker-nomad:local packer init -only=nomad-cluster.null.null /project_root/packer/nomad
   ```

4. Validate the configuration
   ```bash
   docker run \
   --rm \
   -v ${PWD}:/project_root \
   hashistack/docker-nomad:local packer validate -only=nomad-cluster.null.null /project_root/packer/nomad
   ```
5. Run the builder to renew nomad tls certificates in 1password (if required).
   ```bash
   docker run \
   --rm \
   -v ${PWD}:/project_root \
   hashistack/docker-nomad:local packer build -only=nomad-tls.null.null /project_root/packer/nomad

6. Run the builder to provision the nomad nodes.
   ```bash
   docker run \
   --rm \
   --privileged \
   --device /dev/net/tun:/dev/net/tun \
   -e TAILSCALE_AUTHKEY={docker-exec-tailscale-auth-key} \
   -v ${PWD}:/project_root \
   hashistack/docker-nomad:local packer build -only=nomad-cluster.null.null /project_root/packer/nomad
   ```

**Notes:**
- There are various configurations pulled from 1password. Ensure they exist and have been configured running the build.
- If its required to view additional logs, you can pass the `PACKER_LOG` environment variable to the `docker run` command: `-e PACKER_LOG=1`.