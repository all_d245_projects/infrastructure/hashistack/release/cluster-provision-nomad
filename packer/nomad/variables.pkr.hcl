#> common
variable "project_root" {
  type = string
}

variable "playbook_tags" {
  type = string
  default = "all"
}
variable "playbook_skip_tags" {
  type = string
  default = ""
}

variable "purge_certificate_temporary_directory" {
  type = string
  default = "TRUE"
}

variable "one_step_certificate_ca_url" {
  type      = string
}

variable "one_password_service_account_read_token" {
  type      = string
  sensitive = true
}
variable "one_password_service_account_write_token" {
  type      = string
  sensitive = true
}
variable "one_password_vault_id" {
  type      = string
  sensitive = true
}
variable "one_password_inventory_item_id" {
  type      = string
  sensitive = true
}
variable "one_password_hashistack_ca_id" {
  type      = string
  sensitive = true
}
variable "one_password_nomad_server_crt_id" {
  type      = string
  sensitive = true
}
variable "one_password_nomad_server_key_id" {
  type      = string
  sensitive = true
}
variable "one_password_nomad_client_crt_id" {
  type      = string
  sensitive = true
}
variable "one_password_nomad_client_key_id" {
  type      = string
  sensitive = true
}
variable "one_password_nomad_cli_crt_id" {
  type      = string
  sensitive = true
}
variable "one_password_nomad_cli_key_id" {
  type      = string
  sensitive = true
}
variable "one_password_consul_connect_ca_id" {
  type      = string
  sensitive = true
}

variable "tailscale_authkey" {
  type      = string
  sensitive = true
}
variable "tailscale_up_skip" {
  type = bool
}
variable "tailscale_state" {
  type = string
}
variable "tailscale_args" {
  type = string
}

variable "nomad_debug" {
  type = bool
}
variable "nomad_log_level" {
  type = string
}
variable "nomad_network_interface" {
  type = string
}
variable "nomad_force_install" {
  type = bool
}
variable "nomad_server_join" {
  type = set(string)
}
#<

#> server
variable "nomad_env_vars" {
  type = set(string)
}
variable "nomad_server_env_vars" {
  type = set(string)
}
variable "nomad_client_env_vars" {
  type = set(string)
}
#<

#> client
variable "nomad_client_host_networks" {
  type = list(map(string))
}
variable "nomad_client_host_volumes" {
  type = list(map(string))
}
#<