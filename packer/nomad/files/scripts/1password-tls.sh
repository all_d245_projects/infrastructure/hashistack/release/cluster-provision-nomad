#!/usr/bin/env bash

set -o errtrace -o nounset -o pipefail

#vars
purge_certs=$PURGE_CERTS

nomad_certificate_tmp_directory=$PROJECT_ROOT/output/nomad/tls

nomad_server_crt_file_path=$nomad_certificate_tmp_directory/server_crt
nomad_server_key_file_path=$nomad_certificate_tmp_directory/server_key
nomad_client_crt_file_path=$nomad_certificate_tmp_directory/nomad_crt
nomad_client_key_file_path=$nomad_certificate_tmp_directory/nomad_key
nomad_cli_crt_file_path=$nomad_certificate_tmp_directory/cli_crt
nomad_cli_key_file_path=$nomad_certificate_tmp_directory/cli_key
hashistack_ca_file_path=$nomad_certificate_tmp_directory/hashistack_ca

one_step_certificate_ca_url=$ONE_STEP_CERTIFICATE_CA_URL

one_password_vault_id=$OP_HASHISTACK_VAULT_ID
one_password_nomad_server_crt_id=$ONE_PASSWORD_NOMAD_SERVER_CRT_ID
one_password_nomad_server_key_id=$ONE_PASSWORD_NOMAD_SERVER_KEY_ID
one_password_nomad_client_crt_id=$ONE_PASSWORD_NOMAD_CLIENT_CRT_ID
one_password_nomad_client_key_id=$ONE_PASSWORD_NOMAD_CLIENT_KEY_ID
one_password_nomad_cli_crt_id=$ONE_PASSWORD_NOMAD_CLI_CRT_ID
one_password_nomad_cli_key_id=$ONE_PASSWORD_NOMAD_CLI_KEY_ID
one_password_hashistack_ca_id=$ONE_PASSWORD_HASHISTACK_CA_ID

certificate_expires_in_check="24h"

init() {
  mkdir -p $nomad_certificate_tmp_directory
}

fetch_certs_from_1p() {
  echo "fetching hashistack ca from 1password..."
  op document get \
  $one_password_hashistack_ca_id \
  --force \
  --vault $one_password_vault_id \
  --out-file="$hashistack_ca_file_path"

  echo "fetching nomad server crt from 1password..."
  op document get \
  $one_password_nomad_server_crt_id \
  --force \
  --vault $one_password_vault_id \
  --out-file="$nomad_server_crt_file_path"

  echo "fetching nomad server key from 1password..."
  op document get \
  $one_password_nomad_server_key_id \
  --force \
  --vault $one_password_vault_id \
  --out-file="$nomad_server_key_file_path"

  echo "fetching nomad client crt from 1password..."
  op document get \
  $one_password_nomad_client_crt_id \
  --force \
  --vault $one_password_vault_id \
  --out-file="$nomad_client_crt_file_path"

  echo "fetching nomad client key from 1password..."
  op document get \
  $one_password_nomad_client_key_id \
  --force \
  --vault $one_password_vault_id \
  --out-file="$nomad_client_key_file_path"

  echo "fetching nomad cli crt from 1password..."
  op document get \
  $one_password_nomad_cli_crt_id \
  --force \
  --vault $one_password_vault_id \
  --out-file="$nomad_cli_crt_file_path"

  echo "fetching nomad cli key from 1password..."
  op document get \
  $one_password_nomad_cli_key_id \
  --force \
  --vault $one_password_vault_id \
  --out-file="$nomad_cli_key_file_path"  
}

cert_needs_renewal() {
  crt_file_path=$1

  step certificate needs-renewal \
  $crt_file_path \
  --expires-in $certificate_expires_in_check
}

renew_certs() {
  crt_file_path=$1
  key_file_path=$2

  step ca renew \
  $crt_file_path \
  $key_file_path \
  --force \
  --root $hashistack_ca_file_path \
  --ca-url $one_step_certificate_ca_url
}

push_certs_to_1p() {
  crt_label=$1
  op_crt_id=$2
  crt_file_path=$3
  echo "pushing $crt_label to 1password..."
  op document edit \
  $op_crt_id \
  $crt_file_path \
  --vault $one_password_vault_id
}

cleanup() {
  if [[ -n "$purge_certs" ]] && [[ "${purge_certs}" == "TRUE" ]]; then
    echo "deleting certificate directory at $nomad_certificate_tmp_directory..."
    rm -fr $nomad_certificate_tmp_directory
  else
    echo "WARNING: CERTIFICATES HAVE NOT BEEN PURGED."
  fi
}

## MAIN ##
init

fetch_certs_from_1p

# check if server certs need renewal
cert_needs_renewal $nomad_server_crt_file_path
if [ $? -eq 0 ]; then
    echo "Renewing nomad server certificate..."

    renew_certs $nomad_server_crt_file_path $nomad_server_key_file_path

    push_certs_to_1p "nomad crt" $one_password_nomad_server_crt_id $nomad_server_crt_file_path
fi

# check if client certs need renewal
cert_needs_renewal $nomad_client_crt_file_path
if [ $? -eq 0 ]; then
    echo "Renewing nomad client certificate..."

    renew_certs $nomad_client_crt_file_path $nomad_client_key_file_path

    push_certs_to_1p "nomad crt" $one_password_nomad_client_crt_id $nomad_client_crt_file_path
fi

# check if cli certs need renewal
cert_needs_renewal $nomad_client_crt_file_path
if [ $? -eq 0 ]; then
    echo "Renewing nomad cli certificate..."

    renew_certs $nomad_cli_crt_file_path $nomad_cli_key_file_path

    push_certs_to_1p "nomad crt" $one_password_nomad_cli_crt_id $nomad_cli_crt_file_path
fi

cleanup