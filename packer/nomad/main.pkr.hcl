packer {
  required_plugins {
    ansible = {
      source  = "github.com/hashicorp/ansible"
      version = "~> 1"
    }
  }
}

build {
  name = "nomad-tls"
  description = "Build to manage tls certificates for the nomad cluster"
  sources = ["null.null"]

  provisioner "shell-local" {
    script = "${path.root}/files/scripts/1password-tls.sh"
    env = {
      PROJECT_ROOT                     = var.project_root
      PURGE_CERTS                      = var.purge_certificate_temporary_directory
      OP_SERVICE_ACCOUNT_TOKEN         = var.one_password_service_account_write_token
      OP_HASHISTACK_VAULT_ID           = var.one_password_vault_id
      ONE_STEP_CERTIFICATE_CA_URL      = var.one_step_certificate_ca_url
      ONE_PASSWORD_HASHISTACK_CA_ID    = var.one_password_hashistack_ca_id
      ONE_PASSWORD_NOMAD_SERVER_CRT_ID = var.one_password_nomad_server_crt_id
      ONE_PASSWORD_NOMAD_SERVER_KEY_ID = var.one_password_nomad_server_key_id
      ONE_PASSWORD_NOMAD_CLIENT_CRT_ID = var.one_password_nomad_client_crt_id
      ONE_PASSWORD_NOMAD_CLIENT_KEY_ID = var.one_password_nomad_client_key_id
      ONE_PASSWORD_NOMAD_CLI_CRT_ID    = var.one_password_nomad_cli_crt_id
      ONE_PASSWORD_NOMAD_CLI_KEY_ID    = var.one_password_nomad_cli_key_id
    }
  }
}

build {
  name = "nomad-cluster"
  description = ""
  sources = ["null.null"]

  provisioner "shell-local" {
    script = "${path.root}/files/scripts/1password-cluster.sh"
    env = {
      PROJECT_ROOT             = var.project_root
      OP_SERVICE_ACCOUNT_TOKEN = var.one_password_service_account_read_token
      OP_INVENTORY_DOCUMENT_ID = var.one_password_inventory_item_id
      OP_HASHISTACK_VAULT_ID   = var.one_password_vault_id
    }
  }

  provisioner "ansible" {
    playbook_file   = "${path.root}/../../ansible/playbooks/nomad/playbook.yml"
    inventory_file  = "${path.root}/../../output/nomad/inventory"
    galaxy_file     = "${path.root}/../../ansible/requirements.yml"
    roles_path      = "${path.root}/../../ansible/roles"
    user            = "hashistack"
    use_proxy       = "true"
    ansible_env_vars = [
      "ANSIBLE_ROLES_PATH=${path.root}/../../ansible/roles",
    ]
    extra_arguments = [
      "-e packer_vars_json=${jsonencode(var)}",
      "--tags=${var.playbook_tags}",
      "--skip-tags=${var.playbook_skip_tags}"
    ]
  }
}