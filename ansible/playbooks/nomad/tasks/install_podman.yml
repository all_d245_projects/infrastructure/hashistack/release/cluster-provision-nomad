---

- block:
  - name: Remove Docker packages
    apt:
      pkg: "{{ item }}"
      state: absent
    with_items:
      - docker-ce
      - docker-ce-cli
      - containerd.io
      - docker-buildx-plugin
      - docker-compose-plugin
      - docker-ce-rootless-extras
    
  - name: Remove Docker configuration files and data
    file:
      path: "{{ item }}"
      state: absent
    with_items:
      - /var/lib/docker
      - /etc/docker

  - name: Remove Docker repository file
    file:
      path: "/etc/apt/sources.list.d/docker.list"
      state: absent

- name: Install Podman
  apt:
    install_recommends: false
    pkg: podman

- block:
  - name: Create temporary directory for install
    tempfile:
      state: directory
      prefix: ansible-nomad-podman.
    register: nomad_podman_temp_dir

  - name: Create unarchive location
    file:
        dest: "{{ nomad_podman_temp_dir.path }}/install_files"
        state: directory
        owner: "{{ nomad_user }}"
        group: "{{ nomad_group }}"

- name: Check Nomad Podman package checksum file
  stat:
    path: "{{ nomad_podman_temp_dir.path }}/nomad_podman_{{ nomad_podman_version }}_SHA256SUMS"
  tags: installation
  register: nomad_podman_checksum

- name: Get Nomad Podman package checksum file
  get_url:
    url: "{{ nomad_podman_checksum_file_url }}"
    dest: "{{ nomad_podman_temp_dir.path }}/nomad_podman_{{ nomad_podman_version }}_SHA256SUMS"
  tags: installation
  when: not nomad_podman_checksum.stat.exists | bool

- name: Get Nomad Podman package checksum
  shell: |
    set -o pipefail
    grep "{{ nomad_podman_pkg }}" "{{ nomad_podman_temp_dir.path }}/nomad_podman_{{ nomad_podman_version }}_SHA256SUMS"  | awk '{print $1}'
  args:
    executable: /bin/bash
  register: nomad_podman_sha256
  tags: installation

- name: Check Nomad Podman package file
  stat:
    path: "{{ nomad_podman_temp_dir.path }}/{{ nomad_podman_pkg }}"
  register: nomad_podman_package

- name: Download Nomad Podman
  get_url:
    url: "{{ nomad_podman_zip_url }}"
    dest: "{{ nomad_podman_temp_dir.path }}/{{ nomad_podman_pkg }}"
    checksum: "sha256:{{ nomad_podman_sha256.stdout }}"
    timeout: "42"
  tags: installation
  when: not nomad_podman_package.stat.exists

- name: Unarchive Nomad Podman
  unarchive:
    remote_src: true
    src: "{{ nomad_podman_temp_dir.path }}/{{ nomad_podman_pkg }}"
    dest: "{{ nomad_podman_temp_dir.path }}/install_files/"
    creates: "{{ nomad_podman_temp_dir.path }}/install_files/nomad-driver-podman"
  tags: installation

- name: Install Nomad Podman
  copy:
    remote_src: true
    src: "{{ nomad_podman_temp_dir.path }}/install_files/nomad-driver-podman"
    dest: "{{ nomad_plugin_path }}"
    owner: "{{ nomad_user }}"
    group: "{{ nomad_group }}"
    mode: 0755
  tags: installation
  notify: restart nomad

- name: Start podman service
  service:
    name: podman
    enabled: true
    state: started

- name: Cleanup
  file:
    path: "{{ nomad_podman_temp_dir.path }}"
    state: "absent"
  tags: installation